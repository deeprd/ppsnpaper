
%%%%%%%%%%%%%%%%%%%%%%% file typeinst.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is the LaTeX source for the instructions to authors using
% the LaTeX document class 'llncs.cls' for contributions to
% the Lecture Notes in Computer Sciences series.
% http://www.springer.com/lncs       Springer Heidelberg 2006/05/04
%
% It may be used as a template for your own input - copy it
% to a new file with a new name and use it as the basis
% for your article.
%
% NB: the document class 'llncs' has its own and detailed documentation, see
% ftp://ftp.springer.de/data/pubftp/pub/tex/latex/llncs/latex2e/llncsdoc.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass[runningheads,a4paper]{llncs}
\usepackage{listings}
\usepackage{caption}
\usepackage{amssymb}
\usepackage{amsmath}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{algorithm} 
\usepackage{algorithmic}

\newtheorem{defi}{Definition}
\usepackage{booktabs}
\usepackage{url}
\urldef{\mailsa}\path|{axj006, j.e.rowe, c.zarges}@cs.bham.ac.uk|    
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{An Immune-Inspired Algorithm for the Set Cover Problem}

% a short form should be given in case it is too long for the running head
\titlerunning{An Immune-Inspired Algorithm for the Set Cover Problem}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Ayush Joshi\and Jonathan E. Rowe\and Christine Zarges}
%
\authorrunning{An Immune-Inspired Algorithm for the Set Cover Problem}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{School of Computer Science, University of Birmingham,\\
Edgbaston, Birmingham, UK\\
\mailsa}
%\url{http://www.cs.bham.ac.uk}}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{Lecture Notes in Computer Science}
\tocauthor{Authors' Instructions}
\maketitle


\begin{abstract}
This paper introduces a novel parallel immune-inspired algorithm based on recent developments in the understanding
of the germinal centre reaction in the immune system. Artificial immune systems are relatively new randomised search heuristics and 
work on parallelising them is still in its infancy. We compare our algorithm with a parallel
implementation of a simple multi-objective evolutionary algorithm on benchmark instances of the set cover problem taken from the OR-library.
We show that our algorithm finds feasible solutions faster than the evolutionary algorithm using less parameters
%as well as less
and
communication effort.

\keywords Artificial immune systems, \textsc{GSEMO}, set cover
\end{abstract}


\section{Introduction}
Artificial immune systems (AIS) are randomised search heuristics inspired from the immune system of vertebrates~\cite{de2002artificial}. Unlike any other biological system, the immune 
system has several desirable properties combined together, which make it a great inspiration 
for the design of randomised search heuristics. Due to properties like diversity, robustness,
and memory, algorithms inspired by the immune system have been applied to a large number of applications such as machine learning, security, 
robotics, and optimisation~\cite{de2002artificial}. %gives a detailed survey of applications. 

As more and more problems in the real world are getting increasingly complex the 
approaches to solve these are unable to scale and maintain robustness~\cite{greensmith2007dendritic,kim2001towards}. Natural 
processes on the other hand are robust and perform complicated tasks well, thus
it is hoped that understanding and using more detailed ideas from these systems will
help us design better systems. In this direction some work has been done
by Greensmith~\cite{greensmith2007dendritic} on the dendritic cell algorithm 
but this has been limited to intrusion detection and classification. Sim et al.~\cite{sim2014lifelong}
have proposed a hyper-heuristic called NELLI which learns from changing problem 
landscapes and has been shown to perform better than single human-designed heuristics.

In recent decades with the advancements in technology parallel architectures and
multi-processor systems are becoming more and more common. As a consequence parallelisation 
of evolutionary algorithms (EA), in order to better utilise available resources and save time,
is gaining importance and popularity~\cite{alba2005parallel}. EAs are 
inherently suitable for parallel implementations as operations
such as fitness calculations and mutations can be performed on separate processors. Parallel EAs 
based on island models, where each island is an independent population evolving on
a separate processor, have features like inherent diversity. Some communication is 
required to guide the search process and this is achieved by exchange of solutions 
between islands.

We propose a novel artificial immune algorithm, \textit{the germinal centre 
artificial immune system} (\textsc{GC-AIS}) which has been developed
based on recent understanding of the germinal centre reaction in the immune system~\cite{zhang2013germinal}.
This new model has interesting properties like a dynamic population of islands and inherent parallelism. 
We compare it with a parallel version of the global simple evolutionary multi-objective optimiser (GSEMO)
\cite{mambrini2012homogeneous} on instances of the set cover problem taken from the OR-library~\cite{beasley1990or}. It is shown that
the \textsc{GC-AIS} is able to reach the feasible solution region faster than the homogeneous island model with less 
communication effort as well as less parameters to be set manually.

The outline of the paper is as follows: In Section~\ref{sec:preliminaries} some preliminary information about the parallel \textsc{GSEMO} 
is provided along with the problem description. Section~\ref{sec:gcais} gives the description of the \textsc{GC-AIS} model along with 
its pseudo-code. In Section~\ref{sec:experiments} the experimental set-up along with the obtained results are provided. The paper is concluded in Section~\ref{sec:discussion} with a discussion on the observed results and conclusions thereafter. 


\section{Preliminaries}\label{sec:preliminaries}

The set cover problem (SCP) can be defined as follows: Given a \textit{universe set} $U$ consisting of $m$ items and a 
set $S$ of $n$ subsets of $U$ whose union equals $U$, the goal is to find the smallest subset of $S$ that covers the whole universe $U$. More formally:

\begin{defi}
Let the set of $ m $ items $U:=\{u_{1},...,u_{m}\}$
denote the universe and let $S:=\{s_{1},...,s_{n}\}$ such that $ s_{i} \subseteq U $ for $ 1 \leq i \leq n  $ and  $\bigcup_{i=1}^{n}s_{i}=U$.  The unicost set cover problem can be defined as 
finding a selection $I \subseteq \{1,2,...,n\}$ such that $ \bigcup_{k \in I}{s_{k}} = U $ with minimum $\vert{I}\vert$ .
\end{defi}

SCP is a NP-hard combinatorial optimisation problem with many practical applications, one of the most important being scheduling~\cite{caprara2000algorithms}. A survey of techniques used to solve the set cover problem can be found in~\cite{caprara2000algorithms}. The description of SCP above is a constrained single objective problem where the objective is to find the smallest subset of S which covers the universe and the constraint is that the subset covers the universe.

We convert this to a multi-objective version by using the constraint as a secondary objective~\cite{friedrich2010approximating}. Let vector $A = a_{1}a_{2}\ldots a_{n} \in \{0,1\}^n$ denote a solution where $ a _{i} = 1$ if set $s_{i}$ is in the solution and $ 0 $ 
otherwise. Let $N$ equal the number of sub sets selected in  $A$, and  let $C$ equal the number of elements left uncovered in $U$. The fitness function $V$ for the SCP can now be written as a vector $V = \langle C,N \rangle$.

Multi-objective optimisation~\cite{deb2001EMO} is the task of finding optimal solutions to a 
problem which has several objectives, often competing with each other. A solution is said to dominate another if it is better in at least one
objective and has at least the same fitness for the other objectives. 
In this case it is not always possible to order all individuals in a population since 
two potential solutions may each be good in a different objective and worse in the others. 
Therefore, there is not necessarily a unique optimal solution but a set of solutions where each member is not dominated by any other solution in the search space. This set is called the Pareto set and its image in the objective space is called the Pareto front.

The {\it global simple evolutionary multi-objective optimiser} (\textsc{GSEMO})~\cite{giel2003evolutionary} is a generalisation of the $(1+1)$ EA for multi-objective optimisation. It maintains a set of non-dominated solutions in every iteration.  The parallel variant of \textsc{GSEMO} called 
the homogeneous island model \textsc{GSEMO} based on~\cite{mambrini2012homogeneous} can be described as a collection 
of $ \mu $ islands which are fully connected to each other where each island runs
an independent instance of the \textsc{GSEMO} algorithm. We refer to this model as parallel \textsc{GSEMO} (\textsc{PGSEMO}) throughout 
this paper. This is described in Algorithm~\ref{algo:gsemo}.

\begin{algorithm}[tbh]
\caption{ Parallel \textsc{GSEMO} based on homogeneous island model~\cite{mambrini2012homogeneous}}\label{algo:gsemo}
\begin{algorithmic}
\STATE \textbf{Let} $P^{t}_{i}$ denote the population in each island $i$ at generation $t$, $\mu$ denote number 
   of islands, and $p$ denote probability of communication.
\STATE Initialise $P^{0}=\{P_{1}^{0},\ldots,P_{\mu}^{0}\}$ where $P_{i}^{0}=\{0^{n}\}$ for $1\leq i\leq\mu$. Let $t:=0$.
\LOOP
\FOR{each island $i$ in parallel}
\STATE Select an individual $x$ from $P^{t}_{i}$ uniformly at random.
\STATE Create offspring $x'$ by mutating $x$ with standard bit mutation, i.\,e., flip each bit with probability $1/n$.
\IF{any individual in $P^{t}_{i}$ dominates $x'$}
\STATE Leave $P^{t}_{i}$ unchanged.
\ELSE
\STATE Remove all individuals dominated by $x'$ from $P^{t}_{i}$ and add $x'$ to $P^{t}_{i}$.
\ENDIF
\STATE  With probability $p$ send copy of population $P^{t}_{i}$ to all $\mu-1$ neighbours. 
\STATE  Combine $P^{t}_{i}$ with copies of populations received from neighbours.
\STATE  Remove all dominated solutions from $P^{t}_{i}$ and let $P^{t+1}_{i} = P^{t}_{i}$.
\ENDFOR
\STATE  \textbf{Let} $t=t+1$.
\ENDLOOP
\end{algorithmic}
\end{algorithm}

Communication effort can be described as the number of individuals which have been 
exchanged between the islands. We define the total \textit{communication effort} as  
the total number of individuals which were transmitted in one run of the algorithm and the \textit{parallel running time} as the number of generations it takes for the algorithm to reach an optimal solution~\cite{mambrini2012homogeneous}.

\section{The GC-AIS Algorithm }\label{sec:gcais}

In the immune system~\cite{murphy2011janeway} \textit{germinal centres} (GC) are regions where the invading \textit{antigen} (Ag) is presented 
to the {\it B cells} (a kind of immune cell) which create {\it antibodies} (Ab) that in turn bind to the pathogen 
in order to eradicate it. At the start of the invasion, the number of GCs grows and they try to find the best \textit{Ab}
for the pathogen by continuously mutating and selecting the B cells which can bind with the
pathogen. Periodically GCs communicate by transmitting their $Ab$s to other GCs. By proliferation, mutation and selection of immune cells this GC reaction is able 
to produce $Ab$s which can eradicate the pathogen. Towards this stage the number of GCs starts declining. %The detailed
%mechanisms involved in the immune system and germinal centre reaction are explained in~\cite{murphy2011janeway}.

The exact mechanism of selection in the GC is an active area of research and a new theory forms the basis of our algorithm~\cite{zhang2013germinal}. According to this work the selection of B cells to be kept alive for proliferation, is maintained by the B cells themselves as they secrete $Ab$ which bind with $Ag$ and in turn 
directly compete with other B cells to bind with $Ag$. This is an inter-GC phenomenon as $Ab$ from one
GC can migrate to others and the competition increases which can lead to disappearance of GCs which can not cope up with the $Ab$ from other GCs.

The motivation to apply the \textsc{GC-AIS} to the 	set cover problem comes from our belief that in an abstract way the 
immune system tries to solve the 	set cover problem. Every time the body is invaded by 
a pathogen, the immune system must produce $Ab$s which are able to bind with the 
antigen (a partial cover) and must improve this $Ab$ by optimisation so that the binding is strong enough to eradicate it (full cover).
So if we visualise a possible pathogen binding site as an instance of the universe set, and the binding regions
of the B cells as possible solutions, then the immune system tries to solve the problem
of finding the best match to the pathogen, by randomised variations in the solutions.

The \textsc{GC-AIS} (see Algorithm~\ref{algo:gcais}) starts with one GC which contains one B cell, representing a problem solution.
Offspring are created by standard bit mutation of B cells in GCs.
In the current version of our algorithm we restrict ourselves to GCs that contain only a single B cell.
In every generation there is a migration of $Ab$ between GCs, performed by transmitting only the fitness value of the offspring from one GC to another. After migration, dominated solutions are deleted which can lead to the eradication of a  GC. The surviving offspring form new GCs. This leads to a model where the number of GCs is dynamic in nature.

The \textsc{GC-AIS} always maintains a set of non-dominated solutions in every generation. A parameter for the number of GCs  is not required as the number is dynamic and evolves as the algorithm runs. A preliminary design of the GC-AIS can be found in~\cite{joshi2013design}. 

\begin{algorithm}[tbh]
\caption{The \textsc{GC-AIS}}\label{algo:gcais}
\begin{algorithmic}
\STATE \textbf{Let} $G^t$ denote the population of GCs at generation $t$ and $g^{t}_{i}$ the $i$-th GC in $G^t$.
\STATE Create GC pool $G^0= \{g_{1}^{0}\}$ and initialise $g_{1}^{0}$. Let $t: = 0$.
\LOOP
\FOR{each GC $g^{t}_{i}$ in pool $G^t$ in parallel}
\STATE Create offspring $y_i$ of individual $g^{t}_{i}$ by standard bit mutation.
\ENDFOR
%\STATE Let $$
\STATE Add all $y_i$ to $G^{t}$, remove all dominated solutions from $G^{t}$ and let $G^{t+1}=G^{t}$.
%\STATE From the set of all GC and offspring remove the dominated individuals.
%\STATE Create a new GC for each offspring that is not dominated by any other offspring.
\STATE  \textbf{Let} $t=t+1$.
\ENDLOOP
\end{algorithmic}
\end{algorithm}

\section{Experimental Results}\label{sec:experiments}

In this section we present the results obtained on running the \textsc{GC-AIS} and 
the \textsc{PGSEMO} on some benchmark test instances of the SCP. The instances
are selected from the SCP test bed of the OR-library~\cite{beasley1990or} where the instances
are grouped into classes based on the size of the problem. One instance each
was selected randomly from the 12 problem classes named 4, 5, 6, A, B, C, D, E, RE, RF, RG and RH. 


The \textsc{PGSEMO} requires the number of islands
and the probability of communication to be set manually while  \textsc{GC-AIS} does not require these parameters. As in~\cite{mambrini2012homogeneous}, both algorithms initialise individuals in $0^{n}$. This was done as most problem specific algorithms use this method. For the GC-AIS we observed that starting from a random string gives poor results. 
The stopping criteria can be based on a fixed budget of generations or letting the algorithms run until a certain desired fitness is achieved. 

The probability of communication in the \textsc{PGSEMO} was initially based on the equation $p = \mu/mn$, where $\mu$ is the number of islands, $p$ is the probability of communication and $m$ and $n$ describe the problem size. This value gives the best performance guarantee for a complete topology~\cite{mambrini2012homogeneous}. To estimate the number of islands $\mu$  for the \textsc{PGSEMO}, \textsc{GC-AIS} was run for 10,000 generations and it was observed that sufficiently good solutions were achieved. The average of the maximum number of islands in 30 runs was used 
as the number of islands for \textsc{PGSEMO}. This is done so that a fair comparison can be made in terms of computation resources available to both algorithms. Due to space restrictions it is not 
possible to include plots for every instance in this paper, key representative plots are provided.


The first set of experiments was performed to analyse the solution quality both algorithms can achieve using a fixed budget of fitness evaluations. A quota of 7500 generations was set as a stopping criteria and 
average fitness achieved per generation was plotted. This can be seen in Figures~\ref{scp41} and \ref{b4nre}. It was observed experimentally that using $p = \mu/mn$ to set $p$ resulted in 
sub-par performance and experiments were tried with higher rates of communication, $p = 1/n$ , $p = 1/m$ and $p = 1/\mu$. These are
shown for problem $scp41$ in Figure~\ref{scp41}. We observed that having the probability of communication $p = 1/\mu$, so that on average every generation one island communicates gives best results. 


\begin{figure}[tbh]
\centerline{\includegraphics[trim=0mm 70mm 0mm 70mm,clip,width=1\textwidth]{1.pdf}}  
\caption{Fitness plots for \textsc{GC-AIS} and \textsc{PGSEMO} for problem $scp41$ with standard deviation as shaded error bars, averages performed over 30 independent runs.} \label{scp41}
\end{figure}

\begin{figure}[tbh]
\includegraphics[trim=0mm 70mm 0mm 70mm,clip,width=1\textwidth]{2.pdf}  
\caption{Fitness plots for \textsc{GC-AIS} and \textsc{PGSEMO} for problem $scpb4$ and $scnpnre1$ with standard deviation as shaded error bars, averages performed over 30 independent runs.}  \label{b4nre}
\end{figure}

Figures~\ref{scp41} and \ref{b4nre} show the fitness achieved per generation in both the \textsc{GC-AIS} and the \textsc{PGSEMO}. The 
dotted lines depict the average number of sets used and the solid lines depict the average number of uncovered elements per generation. The standard deviation per generation can be seen as the shaded error-bars. To better see the difference between the curves in the early phase, the plots have been zoomed in, which can be seen as the smaller sub-plots inside these figures.

\begin{figure}[tbh]
\centerline{\includegraphics[trim=0 125mm 20mm 75mm,clip,width=1\textwidth]{finalboth.pdf}}  
\caption{Total communication cost until generation $t$, for problems $scp41$ and $scpnre1$ averaged for 30 runs, for \textsc{GC-AIS} and \textsc{PGSEMO}.}  \label{comm}
\end{figure}

For our next set of experiments we are interested in finding the generations required to reach a fixed fitness value. From our previous experiment we use the best fitness value which has been achieved in every run. Average generations to reach this value were computed and the Wilcoxon rank-sum test was performed. We additionally compare the results achieved by PGESMO and GC-AIS with the best known results and the results of a simple Greedy heuristic~\cite{grossman1997computational,musliu2006local}. All results are shown in Table~\ref{table}. In 8 out of the 12 rows of the table it can be seen that there is a significant difference ($p$-value smaller than $0.05$) between the performance of the two algorithms, visible from the Wilcoxon rank-sum test results, these entries have been written in bold face. In 7 out of these 8 cases GC-AIS performed faster than PGSEMO.  

\begin{table}[tbh]
\caption{Generations required to reach a sufficiently good fitness. `AIS' represents generations required 
for GC-AIS, `PGSEMO' represents generations required for PGSEMO, `fitness' is the target fitness value used as stopping criterion. The column `WRStest' shows the $p$-values of the Wilcoxon rank-sum test, `Gr' contains the fitness achieved by the Greedy heuristic, 'known' contains the best known value and 'achieved' contains best value achieved by GC-AIS in the first set of experiments. Generations are averaged over 30 runs.} \label{table}
\centering
\begin{tabular}{ccccccc|ccc}
\toprule
Problem & m $\times$ n & Fitness & $\mu$ & AIS & \textsc{PGSEMO} &  WRStest& Gr & Known & Achieved \tabularnewline
\toprule 
scp41 & 200$\times$1000 & (0,45) & 65 &\textbf{3654.5} & 4349.3 &0.0013	&41&38&41 \tabularnewline
\hline 
scp52 & 200$\times$2000 & (0,43) & 65 &\textbf{3412.4} & 4440.4 &4.1127e-07	&38&34&39  \tabularnewline
\hline 
scp63 & 200$\times$1000 & (0,25) & 45 &2260.8 & 2037.3 		& 0.3112 &21&21&22 \tabularnewline
\hline 
scpa5 & 300$\times$3000 & (0,50) & 75 &\textbf{3518.8} & 4702.8 &9.2603e-09	&43&38&44  \tabularnewline
\hline 
scpb4 & 300$\times$3000 & (0,28) & 50 & 2941.2 & 2682.3 	& 0.0575 &24&22&25 \tabularnewline
\hline 
scpc3 & 400$\times$4000 & (0,58)  & 90 &\textbf{3418.}  & 4765.4 &3.6897e-11	&47&43&51  \tabularnewline
\hline 
scpd2 & 400$\times$4000 & (0,31) & 50&3507.8 & 3450.4 &0.9646	&26&25&28  \tabularnewline
\hline 
scpe1 & 50$\times$500 & (0,5) & 12&\textbf{961.4} & 2051.1 &0.0058 	&5&5&5 \tabularnewline
\hline 
scpnre1 & 500$\times$5000 & (0,22) & 30 &1409 & 1506 &0.1370	&18&17&19 \tabularnewline
\hline 
scpnrf2 & 500$\times$5000 & (0,12) & 20&1867.5 & \textbf{1490.9} &0.0302	&11&10&11  \tabularnewline
\hline 
scpnrg3 & 1000$\times$10000 & (0,87) & 120&\textbf{3168.4}  & 6519.8 &3.0199e-11	&-&62&77  \tabularnewline
\hline 
scpnrh4 & 1000$\times$10000 & (0,44) & 65&\textbf{3179} & 4069 &1.3848e-06		&-&34&40 \tabularnewline
\bottomrule
\end{tabular}

\end{table}

To estimate the communication effort of the two algorithms, we count the number of 
individuals which are exchanged between islands per generation. The plots for the accumulated number
of communications until generation $t$ are shown in Figure~\ref{comm} for the problem instances $scp41$ and $scpbnre4$.



\section{Discussion and Conclusion}\label{sec:discussion}

The \textsc{GC-AIS} is able to reach the region of feasible solutions, i.\,e., solutions where the first objective value is $0$, faster than the \textsc{PGSEMO}. This can be seen
in Figures~\ref{scp41} and \ref{b4nre}: in the first 500-1000 generations the solid curve, which depicts the uncovered elements, can be seen to reach 0 faster for 
\textsc{GC-AIS} than for \textsc{PGSEMO}. It was observed that during 
the generations towards the end, mutations of individuals in the population very rarely replace any parent. 
We think that at this stage that all islands in the PGSEMO converged to a similar Pareto set due to communication over the course of the run,   while there is in fact just one Pareto set for the GC-AIS. Therefore for the PGSEMO, having many parallel islands each with a similar population increases the chance of finding an improvement, in comparison to a single GC population in the GC-AIS. This advantage of having many islands comes at a cost, which is the communication effort. As communication increases the benefits of parallelism begin to fade, as it becomes a substantial time constraint for the overall performance. The GC-AIS requires far less communications over all than PGSEMO which can be seen in Figure~\ref{comm}. GC-AIS also uses less communication information than the PGSEMO, as only fitness values are sent to other GCs in GC-AIS while the whole population is communicated in PGSEMO.   
     
Parameter setting is a big factor when running an algorithm on a new problem. The \textsc{GC-AIS} has a clear advantage
over \textsc{PGSEMO}
in terms of parameters to be set: the PGSEMO needs two parameters $p$ and $ \mu$ to be set manually while GC-AIS does not require any of these.
 As can be seen in Figure~\ref{scp41}, setting the right values for $p$ is crucial 
to obtain the desired performance. The values we found optimal are different from the ones, which give the best proven performance guarantees, as suggested in~\cite{mambrini2012homogeneous}. 

We proposed a novel immune-inspired algorithm called \textsc{GC-AIS} and compared it with a simple multi-objective
evolutionary algorithm. With new ideas taken from the immune system and an interesting motivation to use
the set cover problem as a test, we show that the \textsc{GC-AIS} performs faster, uses less communication and has the advantage of 
not requiring as much human intervention to set it up. In the future we will investigate how 
the \textsc{GC-AIS} performs on other problem classes and compare it with state-of-the-art techniques for 
these problems. 


\bibliographystyle{abbrv}
\bibliography{bib}

\end{document}
